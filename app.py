import numpy as np
import os
import pickle
from flask import Flask, render_template, request
from whoosh import index,qparser
app = Flask(__name__)

@app.route("/")
def home():
    return render_template("home.html")


#prediction function
def ValuePredictor(to_predict_list):
    to_predict = np.array(to_predict_list).reshape(1,23)
    loaded_model = pickle.load(("model.pkl"))
    result = loaded_model.predict(to_predict)
    return result[0]
@app.route('/finalresult',methods = ['POST'])
def result():
    if request.method == 'POST':
            #search query 
            query = request.form['QA']
            #print(query)
            results = []
            ix = index.open_dir("C://Anu//whhosh final//qadata_Index")
            schema = ix.schema
            # Create query parser that looks through designated fields in index
            og = qparser.OrGroup.factory(0.9)
            mp = qparser.MultifieldParser(['question', 'answer'], schema, group = og)
            # This is the user query
            q = mp.parse(request.form['QA'])
            # Actual searcher, prints top 10 hits
            with ix.searcher() as s:
                results = s.search(q, limit = 5)
                #for i in range(5):
                    #print(results[i]['question'], str(results[i].score), results[i]['answer'])
                return render_template("finalresult.html",searchquery=request.form['QA'],
                                       Q1=results[0]['question'],A1=results[0]['answer'],S1=round(results[0].score,2),
                                       Q2=results[1]['question'],A2=results[1]['answer'],S2=round(results[1].score,2),
                                       Q3=results[2]['question'],A3=results[2]['answer'],S3=round(results[2].score,2),
                                       Q4=results[3]['question'],A4=results[3]['answer'],S4=round(results[3].score,2),
                                       Q5=results[4]['question'],A5=results[4]['answer'],S5=round(results[4].score,2)
                                       )


if __name__=='__main__':
   app.run(debug = True)












